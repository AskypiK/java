package cours.TD1.demo;



import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cours.TD1.ressources.Login;
import cours.TD1.ressources.Personne;

public interface RepositoryLogin extends CrudRepository<Login, Long>{
	
	Login save(Login login);
	List<Login> findByPseudoAndmdp(String pseudo,String mdp);
	

}
