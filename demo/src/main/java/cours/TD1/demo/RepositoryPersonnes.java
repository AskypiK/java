package cours.TD1.demo;



import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cours.TD1.ressources.Personne;

public interface RepositoryPersonnes extends CrudRepository<Personne, Long>{
	
	Personne save(Personne personne);
	List<Personne> findByPrenom(String prenom);
	List<Personne> findByNom(String nom);

}
