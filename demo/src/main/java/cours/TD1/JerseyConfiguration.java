package cours.TD1;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import cours.TD1.ressources.MyRessource;

@Component
@ApplicationPath("rest")
@Configuration
public class JerseyConfiguration extends ResourceConfig
{
	public JerseyConfiguration()
	{
		register(MyRessource.class);
		//register(Calculette.class);
		
	}

}
