package cours.TD1.ressources;



import java.util.List;
import java.util.Optional;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;

import cours.TD1.demo.RepositoryLogin;
import cours.TD1.demo.RepositoryPersonnes;

@Path("/Personnes")
public class MyRessource {
	@Autowired
	private RepositoryPersonnes repoPersonnes;
	private RepositoryLogin repoLogin;

	public MyRessource()
	{
		
	}
	
	@GET
	@Produces("application/json")
	@Path("/getPersonnes")
	public Personne getPersonne(@QueryParam("prenom") String prenom,@QueryParam("nom") String nom)
	{
		
		return new Personne(nom,prenom);
		
	}
	
	@POST
	@Produces("application/json")
	@Path("/createPersonne")
	public Personne createPersonne(Personne p)
	{
		return repoPersonnes.save(p);
	}
	
	@GET
	@Produces("application/json")
	@Path("/searchPersonne")
	public List<Personne> searchPersonne(@QueryParam("nom") String nom)
	{
		return repoPersonnes.findByNom(nom);
	}
	
	@DELETE
	@Produces("application/json")
	@Path("/deletePersonne")
	public void deletePersonne(Personne p){
		{
			Optional<Personne> pers = repoPersonnes.findById(p.getId());
			if(pers != null)
			repoPersonnes.deleteById(p.getId());
			else
				System.out.println("erreur");		
		}
	}
	
	
	 	
	 
}
