package cours.TD1.ressources;

import java.util.List;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import lombok.extern.java.Log;


@Path("/Calculette")
public class Calculette {
	
	public Calculette() {
	
	}
	
	@GET
	@Produces("application/json")
	@Path("/Addition")
	public Addition getAddition(@QueryParam("valeurs") List<Float> valeurs)
	{	
		return new Addition(valeurs);
	
	}
	
	@GET
	@Produces("application/json")
	@Path("/Soustraction")
	public float getSoustraction(@QueryParam("valeur1") float v1,@QueryParam("valeur2") float v2)
	{	
		return new Soustraction(v1,v2).getResult();
	
	}

}
