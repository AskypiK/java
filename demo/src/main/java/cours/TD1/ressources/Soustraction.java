package cours.TD1.ressources;

public class Soustraction {
	
	float v1;
	float v2;
	float result;
	public float getV1() {
		return v1;
	}
	public void setV1(float v1) {
		this.v1 = v1;
	}
	public float getV2() {
		return v2;
	}
	public void setV2(float v2) {
		this.v2 = v2;
	}
	public Soustraction(float v1, float v2) {
		
		this.v1 = v1;
		this.v2 = v2;
		calcul(v1, v2);
	}
	public Soustraction() {
		
	}
	
	public void calcul(float v1, float v2)
	{
		result = v1 - v2;
	}
	public float getResult() {
		return result;
	}
	public void setResult(float result) {
		this.result = result;
	}
	

}
