package cours.TD1.ressources;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Login  implements Serializable {
	
	
	
	private static final long serialVersionUID = 4233964682406912703L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	String login;
	String mdp;
	
	public Login(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	

	
}
