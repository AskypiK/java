import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.StyledEditorKit.BoldAction;

public class Applet1 extends JFrame implements MouseListener {

	int x, y, w, h,i,j;
	int m = 40;
	int taille = 5;
	int T[][];
	int Tbool[][];
	int cpt=0;
	int max=0;
	int cpt1=0;
	boolean gagne=false;
	boolean IAgagne=false;
	public Applet1() {
		JTextField txt = new JTextField();
		JOptionPane jo = new JOptionPane();
		String taillestring = jo.showInputDialog(null,"Quelle taille voulez vous ?",JOptionPane.QUESTION_MESSAGE);
		taille = Integer.valueOf(taillestring);
		
		addMouseListener(this);
		setSize(500, 500);
		setVisible(true);
		T = new int[taille][taille];
		Tbool= new int[taille][taille];;
		w = getSize().width - 40;
		h = getSize().height - 40;
		repaint();
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(d.width / 2 - getWidth() / 2, d.height / 2 - getHeight() / 2);
		tableau();
	}

	public void paint(Graphics g) {
		super.paint(g);

		
		int w = getWidth() - m * 2;
		int h = getHeight() - m * 2;
		

		

		g.draw3DRect(40, 40, w, h, true); //Creation du contour
		for (int i = 0; i <= taille; i++) {
			g.drawLine(m, m + h * i / taille, w + m, m + h * i / taille); //on creer les colonnes.
			g.drawLine(m + w * i / taille, m, m + w * i / taille, m + h); //on creer les lignes.
		}
		
		for(int i = 0;i<taille;i++)
		{
			for(int j=0;j<taille;j++)
			{
				if(Tbool[i][j]==1 )
				{
					g.setColor(Color.BLUE);
					g.drawString(String.valueOf(T[i][j]),(w / taille/2)*2 + (w * i / taille),(h  / taille/2)*2+ (h * j / taille)); //on ecrit le nombre � la case correspondantes.
					
				}
				if(Tbool[i][j]==2 )
				{
					g.setColor(Color.RED);
					g.drawString(String.valueOf(T[i][j]), (w / taille/2)*2 + (w * i / taille),(h  / taille/2)*2+ (h * j / taille)); //on ecrit le nombre � la case correspondante.
					
				}
				
			}
		}
		
		
		
	}

	public void affect() {
	//	System.out.println("clicked");
		//System.out.println("x= "+x+"y= "+y);
		int i=0;int j=0;
		int z=0;
		
		for(z=1;z<=taille;z++) {
			if(x < m + w * z / taille && x > m + w * (z-1) / taille) // On cherche le x 
			{
				i=z; 
				
			}
			
			}
			
			for(z=1;z<=taille;z++) {
				if(y < m + w * z / taille && y > m + w * (z-1) / taille) // On cherche le y
				{
					j=z;
					
				}
				
				}
			System.out.println("Vous etes en case ["+(i-1)+"]["+(j-1)+"]"); 
		
	
	
	
		if(Tbool[i-1][j-1]==0)
		{
			Tbool[i-1][j-1]=1;
			AI();
		}
	
		this.i=i-1;
		this.j=j-1;
		this.w = getSize().width - 40;
		this.h = getSize().height - 40;
		
		
		
		
		
		
		
		repaint();
		
		
	}

	public void verification()
	{
		System.out.println(i+"=i  j="+j);
		if(T[i][j]==max && Tbool[i][j]==1) //SI la case trouv� = la valeur max alors on change la valeur de gagne a true.
		{
			gagne=true;
			
	
		}
		if(T[i][j]==max && Tbool[i][j]==2) //SI la case trouv� = la valeur max alors on change la valeur de gagne a true.
		{
			System.out.println("l'ia a win");
			IAgagne=true;
			
	
		}
		
		
		if(gagne==true) //SI l'utilisateur a trouv� le nombre max alors ca affiche le nombre de coups puis ca quitte le programme.
		{
			repaint();
			for(int x1 = 0; x1<taille;x1++)
			{
				for(int x2 = 0; x2<taille;x2++)
				{
					if(Tbool[x1][x2]==1)
						cpt++;
				}			
				}
			JOptionPane.showMessageDialog(this, 
					"vous avez gagn� en "+(cpt)+"coups",
			         " Bravo ",
			         JOptionPane.WARNING_MESSAGE);
			System.exit(0);
			
		}
		if(IAgagne==true) //SI l'utilisateur a trouv� le nombre max alors ca affiche le nombre de coups puis ca quitte le programme.
		{
			repaint();
			for(int x1 = 0; x1<taille;x1++)
			{
				for(int x2 = 0; x2<taille;x2++)
				{
					if(Tbool[x1][x2]==2)
						cpt++;
				}			
				}
			JOptionPane.showMessageDialog(this, 
					"vous avez perdu en "+(cpt)+"coups",
			         " Bravo ",
			         JOptionPane.WARNING_MESSAGE);
			System.exit(0);
			
		}
	}
	public void AI()
	{
		int x =(int) (Math.random() * ( taille - 0 )); // x = nombre random entre 0 et taille d�finie
		int y = (int) (Math.random() * ( taille - 0 )); // y = nombre random entre 0 et taille d�finie
		
		if(Tbool[x][y]==1 || Tbool[x][y]==2 ) //case d�j� remplie
		{
			System.out.println("c'est deja occup�"); 
			AI();
		}
		if(Tbool[x][y]==0) //case vide
		{
			System.out.println("L'IA joue en "+x+" "+y+"\n");
			Tbool[x][y]=2;
			i = x;
			j = y;
			verification();
		}
	}

	
	public void tableau() 
	{
		List<Integer> l = new LinkedList<Integer>(); // creation de la liste
		 
		for(int i=0; i<taille*taille; i++) {
		    l.add(i); //ajout des valeurs dans la liste
		}
		max = l.get(taille*taille-1); //r�cup du max
		System.out.println(max);
		 int nbvaleur= taille*taille; //nombre de valeurs
		Collections.shuffle(l); //m�lange de la liste
		System.out.println(l.toString());
		for(int i=0; i<taille; i++) {	
			for(int j=0; j<taille; j++) {
				System.out.println(j);
			   T[i][j] = l.get(nbvaleur-1); // remplissage du tableau de valeur pour la grille
			  
			   System.out.println("nbvaleur = "+nbvaleur);
			   nbvaleur--;
			   System.out.println("T i"+i+"j"+j+"="+T[i][j]);
			}
		}
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		x=e.getX();
		y=e.getY();
		affect();
		verification(); 
		
		verification();
		
		

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
